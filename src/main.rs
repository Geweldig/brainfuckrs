use std::{env, fs};

fn main() {
    let mut args = env::args().skip(1);
    run(
        fs::read(args.next().expect("Input file not provided!"))
            .expect("Input file does not exist!"),
        args.next().unwrap_or_else(String::new),
    );
}

fn run(program: Vec<u8>, input: String) {
    let mut input = input.chars().map(|x| x as u8);
    let mut tape: [u8; 30000] = [0u8; 30000];
    let mut pc = 0;
    let mut ptr = 0;
    let mut loops = vec![];
    while let Some(x) = program.get(pc) {
        match x {
            b'>' => ptr += 1,
            b'<' => ptr -= 1,
            b'+' => tape[ptr] += 1,
            b'-' => tape[ptr] -= 1,
            b'.' => print!("{}", tape[ptr] as char),
            b',' => tape[ptr] = input.next().unwrap_or_default(),
            b'[' if tape[ptr] == 0 => {
                let mut stack = 1;
                while stack > 0 {
                    pc += 1;
                    match program[pc] {
                        b']' => stack -= 1,
                        b'[' => stack += 1,
                        _ => (),
                    }
                }
            }
            b'[' => loops.push(pc - 1),
            b']' => pc = loops.pop().expect("Impossible loop detected!"),
            _ => (),
        }
        pc += 1;
    }
}
